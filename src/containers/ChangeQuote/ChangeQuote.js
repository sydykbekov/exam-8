import React, {Component} from 'react';
import axios from 'axios';
import Form from '../../components/Form/Form';

class ChangeQuote extends Component {
    state = {
        category: '',
        text: '',
        author: ''
    };

    changeQuote = () => {
        axios.get(`/quotes/${localStorage.getItem('edit')}.json`).then(response => {
            this.setState({category: response.data.category, text: response.data.text, author: response.data.author});
            console.log(this.state);
        });
    };

    changeCategory = (event) => {
        this.setState({category: event.target.value});
    };

    changeAuthor = (event) => {
        this.setState({author: event.target.value});
    };

    changeText = (event) => {
        this.setState({text: event.target.value});
    };

    componentDidMount() {
        this.changeQuote();
    }

    send = () => {
        axios.put(`/quotes/${localStorage.getItem('edit')}.json`, this.state).then(() => {
            this.props.history.push({
                pathname: "/"
            });
        })
    };

    render() {
        if (this.state) {
            return <Form send={this.send} author={this.changeAuthor} text={this.changeText}
                         category={this.changeCategory} selectorValue={this.state.category} state={this.state}/>
        }
        return null;
    }
}

export default ChangeQuote;