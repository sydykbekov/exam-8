import React, {Component} from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";

class CategoriesPage extends Component {
    state = {
        categories: [
            {category: 'All', link: 'all'},
            {category: 'Star Wars', link: 'star-wars'},
            {category: 'Famous people', link: 'famous-people'},
            {category: 'Saying', link: 'saying'},
            {category: 'Humour', link: 'humour'},
            {category: 'Motivational', link: 'motivation'}],
        quotes: [],
        quoteForEdit: {},
        currentCategory: 'All'
    };

    getQuotes = () => {
        axios.get('/quotes.json').then(response => {
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key});
            }
            this.setState({quotes});
        });
    };

    componentDidMount() {
        this.getQuotes();
    }

    editQuote = (id) => {
        this.setState({quoteForEdit: this.state.quotes[id]});
        this.props.history.push(`/quotes/${this.state.quotes[id].id}/edit/`);
    };

    removeQuote = (id) => {
        const quotes = [...this.state.quotes];
        axios.delete(`/quotes/${quotes[id].id}.json`).then(() => {
            this.getQuotes();
        });
    };

    getThisCategory = (index) => {
        if (index === 0) {
            this.getQuotes();
            this.setState({currentCategory: this.state.categories[index]});
        } else {
            axios.get(`/quotes.json?orderBy="category"&equalTo="${this.state.categories[index]}"`).then(response => {
                const quotes = [];
                for (let key in response.data) {
                    quotes.push({...response.data[key], id: key});
                }
                this.setState({quotes, currentCategory: this.state.categories[index]});
            });
        }
    };

    render() {
        return (
            <div className="homePage">
                <ul className="categories-list">
                    {this.state.categories.map((category, key) => {
                        return <NavLink to={`/quotes/${category.link}`}><li key={key} onClick={() => this.getThisCategory(key)}>{category.category}</li></NavLink>
                    })}
                </ul>
                <div className="all-quotes">
                    <h3>{this.state.currentCategory}</h3>
                    {this.state.quotes.map((quote, key) => {
                        return (
                            <div className="quote" key={key}>
                                <p>{quote.text}</p>
                                <h5>- {quote.author}</h5>
                                <i className="fa fa-pencil-square-o" aria-hidden="true" onClick={() => this.editQuote(key)} />
                                <i className="fa fa-times" aria-hidden="true" onClick={() => this.removeQuote(key)} />
                            </div>
                        );
                    })}
                </div>
            </div>
        )
    }
}

export default CategoriesPage;
