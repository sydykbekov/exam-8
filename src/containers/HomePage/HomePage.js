import React, {Component} from 'react';
import axios from 'axios';
import './HomePage.css';
import {NavLink} from "react-router-dom";

class HomePage extends Component {
    state = {
        categories: [
            {category: 'All', link: 'all'},
            {category: 'Star Wars', link: 'star-wars'},
            {category: 'Famous people', link: 'famous-people'},
            {category: 'Saying', link: 'saying'},
            {category: 'Humour', link: 'humour'},
            {category: 'Motivational', link: 'motivation'}],
        quotes: [],
        quoteForEdit: {},
        currentCategory: 'All'
    };

    getQuotes = () => {
        axios.get('/quotes.json').then(response => {
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key});
            }
            this.setState({quotes});
        });
    };

    componentDidMount() {
        this.getQuotes();
    }

    editQuote = (id) => {
        localStorage.removeItem('link');
        localStorage.setItem('edit', this.state.quotes[id].id);
        this.props.history.push({
            pathname: `/quotes/${this.state.quotes[id].id}/edit/`
        });
        this.setState({quoteForEdit: this.state.quotes[id]});
    };

    removeQuote = (id) => {
        const quotes = [...this.state.quotes];
        axios.delete(`/quotes/${quotes[id].id}.json`).then(() => {
            this.getQuotes();
        });
    };

    getThisCategory = (index) => {
        localStorage.setItem('link', `/quotes/${this.state.categories[index].link}`);
        if (index === 0) {
            this.getQuotes();
            this.setState({currentCategory: 'All'});
        } else {
            axios.get(`/quotes.json?orderBy="category"&equalTo="${this.state.categories[index].category}"`).then(response => {
                const quotes = [];
                for (let key in response.data) {
                    quotes.push({...response.data[key], id: key});
                }
                this.setState({quotes, currentCategory: this.state.categories[index].category});
            });
        }
    };

    render() {
        return (
            <div className="homePage">
                <ul className="categories-list">
                    {this.state.categories.map((category, key) => {
                        return <NavLink key={key} to={`/quotes/${category.link}`}><li onClick={() => this.getThisCategory(key)}>{category.category}</li></NavLink>
                    })}
                </ul>
                <div className="all-quotes">
                    <h3>{this.state.currentCategory}</h3>
                    {this.state.quotes.map((quote, key) => {
                        return (
                            <div className="quote" key={key}>
                                <p>{quote.text}</p>
                                <h5>- {quote.author}</h5>
                                <span className="fa edit" onClick={() => this.editQuote(key)}>edit</span>
                                <span className="fa delete" onClick={() => this.removeQuote(key)}>delete</span>
                            </div>
                        );
                    })}
                </div>
            </div>
        )
    }
}

export default HomePage;
