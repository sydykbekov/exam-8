import React, {Component} from 'react';
import axios from 'axios';
import Form from '../../components/Form/Form';

class AddQuote extends Component {
    state = {
        author: '',
        text: '',
        category: ''
    };

    changeCategory = (event) => {
        this.setState({category: event.target.value});
    };

    changeAuthor = (event) => {
        this.setState({author: event.target.value});
    };

    changeText = (event) => {
        this.setState({text: event.target.value});
    };

    sendQuote = () => {
        axios.post('/quotes.json', this.state).then(() => {
            localStorage.removeItem('link');
            this.props.history.push({
                pathname: "/"
            });
        });
    };

    render() {
        return (
            <Form state={this.state} author={this.changeAuthor} text={this.changeText} category={this.changeCategory} send={this.sendQuote}/>
        )
    }
}

export default AddQuote;