import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Header from "./components/Header/Header";
import AddQuote from "./containers/AddQuote/AddQuote";
import HomePage from "./containers/HomePage/HomePage";
import ChangeQuote from "./containers/ChangeQuote/ChangeQuote";

class App extends Component {
    render() {
        const link = localStorage.getItem('link');
        return (
            <Fragment>
                <Route path="/" component={Header} />
                <Switch>
                    <Route path={link ? link : "/"} exact component={HomePage} />
                    <Route path="/add-quote" component={AddQuote} />
                    <Route path="/quotes/:id/edit" component={ChangeQuote} />
                </Switch>
            </Fragment>
        );
    }
}

export default App;
