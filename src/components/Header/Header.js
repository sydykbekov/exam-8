import React, {Component} from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

class Header extends Component {
    removeKey = () => {
        localStorage.removeItem('link');
        this.props.history.push({
            pathname: "/"
        });
    };
    render() {
        return (
            <div className="header">
                <span className="logo" onClick={this.removeKey}>Quotes Central</span>
                <span className="nav-link" onClick={this.removeKey}>Quotes</span>
                <NavLink className="nav-link" to="/add-quote">Submit new quote</NavLink>
            </div>
        )
    }
}

export default Header;