import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <div className="add-quote-container">
            <h3>Submit new quote</h3>
            <h4>Category</h4>
            <select defaultValue={props.selectorValue ? props.selectorValue : "A"} id="selector" onChange={props.category}>
                <option value="A" disabled>Выберите категорию</option>
                <option value="Star Wars">Star Wars</option>
                <option value="Famous people">Famous people</option>
                <option value="Saying">Saying</option>
                <option value="Humour">Humour</option>
                <option value="Motivational">Motivational</option>
            </select>
            <label>
                <h4>Author</h4>
                <input type="text" value={props.authorValue ? props.authorValue : props.state.author} onChange={props.author}/>
            </label>
            <label>
                <h4>Quote</h4>
                <textarea cols="30" rows="10" value={props.textValue ? props.textValue : props.state.text} onChange={props.text} />
            </label>
            <br/>
            <button id="save" onClick={props.send}>Save</button>
        </div>
    )
};

export default Form;